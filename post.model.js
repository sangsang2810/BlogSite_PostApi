// post.model.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Business
let Post = new Schema({
    img: {
        type: String,
        required:true
    },
    title: {
        type: String,
        required:true
    },
    description: {
        type: String,        
        required:true
    },
    author: {
        type: String,
        required:true
    },
    like: {
        type: Number,
        required:true
    }
}, {
    collection: 'post'
});

module.exports = mongoose.model('Post', Post);