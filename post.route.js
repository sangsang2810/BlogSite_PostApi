// post.route.js

const express = require('express');
const postRoutes = express.Router();

// Require Business model in our routes module
let Post = require('./post.model');

// Defined store route
postRoutes.route('/add').post( async function (req, res) {
    let pst = new Post(req.body);
    console.log(pst)
    
    const storePost= await pst.save()
    try{
        console.log('abc')
        res.status(200).json({'post': 'post in added successfully',postNew : pst});
    }
    catch(err){
        res.send(400, err);
    };
});

// Defined get data(index or listing) route
postRoutes.route('/').get(function (req, res) {
    Post.find(function(err, post){
        if(err){
            console.log(err);
        }
        else {
            res.json(post);
        }
    });
});

// Defined edit route
postRoutes.route('/edit/:id').get(function (req, res) {
    let id = req.params.id;
    Post.findById(id, function (err, post){
    res.json(post);
    });
});

//  Defined update route
postRoutes.route('/update/:id').post(function (req, res) {
    
    Post.findById(req.params.id,async function(err, post) {
        if (!post)
            res.status(404).send("data is not found");
        else {
            console.log(post);
            post.img = req.body.img;
            post.title = req.body.title;
            post.description = req.body.description;
            post.author = req.body.author;
            post.like = req.body.like;
            try {
                const postUpdate = await post.save()
                res.json('Update complete');
            }   catch(err) {
                res.status(400).send("unable to update the database");
            };
        }
    });
});

// Defined delete | remove | destroy route
postRoutes.route('/delete/:id').delete(function (req, res) {
    Post.findByIdAndRemove({_id: req.params.id}, function(err, post){
        if(err) res.json(err);
        else res.json('Successfully removed');
    });
});

module.exports = postRoutes;